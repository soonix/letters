#!/usr/bin/env perl
use 5.011; # implies strict + feature 'say'
use warnings;
use Data::Dumper;
binmode STDOUT, ':encoding(cp437)';
use Path::Tiny 'path';


# Wortliste http://www.netzmafia.de/software/wordlists/deutsch.txt
# oder https://github.com/Rohde-Schwarz-Cybersecurity/PanBox/blob/master/panbox-common/test/wortliste-deutsch.txt

sub main {
	my $letters = 'adefilmnoprstuw';
	my $re = qr/^[$letters]+$/i;
	say Dumper $re;
	my @words = path('C:\Users\klaus.baldermann\Downloads\deutsch.txt')->lines({chomp => 1});
	path($letters . '.txt')->spew(map {"$_\n"} grep {$_ =~ $re and $_ !~ /[aou]e/i and $_ !~ /ei/i and $_ !~ /ie/i } @words);
}

main(@ARGV) unless caller();
